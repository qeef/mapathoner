#!/bin/bash
# TEMPORARY, missing _index translation for nl
cp ./hugo/content/_index.md ./hugo/content/_index.nl.md
for lang in `ls ./hugo/content/ | grep _index | cut -d. -f2 | grep -v md`; do
        FI="./hugo/content/credits.${lang}.md"
        if [ ! -f ${FI} ]; then
                echo "Missing credits for ${lang}."
                touch ${FI}
                echo "---" >> ${FI}
                echo "title: \"Credits\"" >> ${FI}
                echo "date: 2018-10-06T00:22:29+02:00" >> ${FI}
                echo "---" >> ${FI}
                echo "# This page is not translated to \"${lang}\" yet" >> ${FI}
                echo "You may [help with translations][1]!" >> ${FI}
                echo "[1]: https://www.transifex.com/josm/josm/josm-plugin_mapathoner-hugo_content_credits/" >> ${FI}
        fi
        FI="./hugo/content/usage.${lang}.md"
        if [ ! -f ${FI} ]; then
                echo "Missing usage for ${lang}."
                touch ${FI}
                echo "---" >> ${FI}
                echo "title: \"Usage\"" >> ${FI}
                echo "date: 2018-10-06T00:22:29+02:00" >> ${FI}
                echo "---" >> ${FI}
                echo "# This page is not translated to \"${lang}\" yet" >> ${FI}
                echo "You may [help with translations][1]!" >> ${FI}
                echo "[1]: https://www.transifex.com/josm/josm/josm-plugin_mapathoner-hugo_content_usage/" >> ${FI}
        fi
done

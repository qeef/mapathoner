package org.openstreetmap.josm.plugins.mapathoner;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import javax.swing.JMenu;
import java.awt.event.KeyEvent;

import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.MainMenu;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;
import org.openstreetmap.josm.spi.preferences.Config;

import org.openstreetmap.josm.plugins.mapathoner.BatchCircleBuildingAction;
import org.openstreetmap.josm.plugins.mapathoner.BatchOrthogonalBuildingAction;
import org.openstreetmap.josm.plugins.mapathoner.BatchLBuildingAction;
import org.openstreetmap.josm.plugins.mapathoner.PickResidentialAreaAction;
import org.openstreetmap.josm.plugins.mapathoner.SelectDuplicateBuildingAction;
import org.openstreetmap.josm.plugins.mapathoner.SelectNonOrthogonalBuildingAction;

/**
 * Mapathoner - some useful tools for HOT and Missing Maps mappers.
 *
 * @author qeef
 * @since xxx
 */
public class MapathonerPlugin extends Plugin
{
    /**
     * Constructs a new {@code MapathonerPlugin}.
     */
    public MapathonerPlugin(PluginInformation info)
    {
        super(info);
        try {
            double user_overlap = Config.getPref().getDouble(
                "mapathoner.duplicate_buildings_min_overlap_percent",
                -1.0);
            if (user_overlap == -1.0) {
                throw new Exception();
            }
        } catch(Exception e) {
            Config.getPref().putDouble(
                "mapathoner.duplicate_buildings_min_overlap_percent",
                50.0);
        }
        MainMenu mm = MainApplication.getMenu();
        JMenu hm = mm.addMenu(
            "Mapathoner",
            tr("Mapathoner"),
            KeyEvent.VK_M,
            mm.getDefaultMenuPos(),
            ht("/Plugin/Mapathoner")
        );

        hm.setMnemonic(KeyEvent.VK_M);

        mm.add(hm, new BatchCircleBuildingAction());
        mm.add(hm, new BatchOrthogonalBuildingAction());
        mm.add(hm, new BatchLBuildingAction());
        hm.addSeparator();
        mm.add(hm, new PickResidentialAreaAction());
        hm.addSeparator();
        mm.add(hm, new SelectDuplicateBuildingAction());
        mm.add(hm, new SelectNonOrthogonalBuildingAction());
    }
}

// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.mapathoner;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Area;
import java.awt.geom.PathIterator;
import java.awt.Rectangle;
import java.lang.Double;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.data.coor.EastNorth;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.Way;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.data.osm.QuadBuckets;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.Notification;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.Geometry;
import org.openstreetmap.josm.tools.ImageProvider;
import org.openstreetmap.josm.tools.Shortcut;

/**
 * Select Duplicate Building
 *
 * <p>
 * Selects duplicate, or near duplicate, area buildings in JOSM's active
 * datalayer.  A "near duplicate" is a building whose area overlaps another
 * building's area by more than 50%. Only the first building encountered of an
 * overlapping pair is selected. This is done so the issue does not have to be
 * looked at twice. The selected buildings are added to the current selection.
 * Currently only works with buildings that are ways (not multipolygons, nor
 * multipart buildings).
 * </p>
 * <p>
 * This script is open source and licensed under GPL.
 * </p>
 *
 * @author Mike Thompson (Github:MikeTho16, OSM: tekim)
 * @author qeef
 * @since xxx
 */
public final class SelectDuplicateBuildingAction extends JosmAction
{
    /**
     * Constructs a new {@code SelectDuplicateBuildingAction}.
     */
    public SelectDuplicateBuildingAction()
    {
        super(
            tr("Select Duplicate Buildings"),
            (ImageProvider) null,
            tr("Selects duplicate, or near duplicate, area buildings."),
            Shortcut.registerShortcut(
                "mapathoner:selectduplicatebuilding",
                tr("Mapathoner: {0}", tr("Select Duplicate Buildings")),
                KeyEvent.VK_X,
                Shortcut.CTRL_SHIFT
            ),
            true,
            "selectduplicatebuilding",
            true
        );
        putValue("help", ht("/Action/SelectDuplicateBuildings"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!isEnabled())
            return;

        DataSet ds = getLayerManager().getEditDataSet();
        if (ds == null)
            return;

        double user_overlap = Config.getPref().getDouble(
            "mapathoner.duplicate_buildings_min_overlap_percent",
            50.0);
        final double minOverlapPercent = user_overlap / 100.0;

        // This not part of the original script, but it is probably a good idea
        // to clear the current selection, otherwise the user will end up with
        // the duplicate buildings selected, PLUS whatever else was selected
        // before the tool was run
        ds.clearSelection();

        // Make a spatial index. If this is not done, it is O(n^2) and will run
        // very slow with large numbers of buildings.
        QuadBuckets<Way> qb = new QuadBuckets<Way>();
        for (Way w: ds.getWays()) {
            if (
                !w.isDeleted()
                && w.isArea()
                && w.hasTag("building")
            ) {
                qb.add(w);
            }
        }

        // Loop through all of the buildings looking for any that overlap
        // Since we only put ways that are tagged as buildings, that are not
        // deleted, and that are closed, into the quadbucket, we don't have
        // to check for those conditions again.
        // Since the Quadbuckets class is a subclass of the Collection class,
        // we can simply iterate over it, thus saving the need to again check
        // if the way is an area and tagged as a building.
        for (Way w1: qb) {
            // No point in checking a building that is already selected.
            if (ds.isSelected(w1))
                continue;
            // Search for any buildings whose bounding box (bbox) intersects
            // with the bbox of w1.  This just means the bbox's intersect, we
            // will still have to check to make sure the buildings themselves
            // intersect, and that the intersection is greater than the
            // specified minimum.
            List<Way> interWays = qb.search(w1.getBBox());
            // Not strictly needed, since we later check if w2 points to the
            // same object as w1, but this does save some processing time,
            // since every way will intersect itself, if only one way was
            // found, we can just ignore it.
            if (interWays.size() < 2)
                continue;

            // Create an Area object representing w1. Note it is in EastNorth
            // (meters) coordinates getAreaEastNorth might work as well?
            Area area1 = Geometry.getArea(w1.getNodes());

            // Compute the area threshold outside the inner loop to save time
            double areaThres = Geometry.closedWayArea(w1) * minOverlapPercent;

            // Check all w2's whose bbox intersects the bbox of w1 to see if
            // there really is an intersection, and if that intersection is
            // greater than the specified minimum percentage.
            for (Way w2: interWays) {
                // A way will always intersect itself, but we are only
                // interested if it intersects another way, so we ignore it.
                if (w2 == w1)
                    continue;
                // If we have a pair of buildings that intersect, we only want
                // to select one of them so we do not create additional items
                // for the user to review.
                if (w2.isSelected())
                    continue;
                // Create an Area object representing the intersection of w1
                // and w2.  Note it is in EastNorth (meters) coordinates
                Area areaInter = Geometry.getArea(w2.getNodes());
                areaInter.intersect(area1);
                double interAreaMeas = 0.0;
                for (Way way: waysFromArea(areaInter)) {
                    interAreaMeas += Geometry.closedWayArea(way);
                }
                // Note that we cannot use Geometry.polygonIntersection(area1,
                // area2, areaThres) != Geometry.PolygonIntersection.OUTSIDE
                // here because the eps (areaThres) parameter is not the
                // threshold in square meters, but the larger of the height or
                // width of the bbox of the intersection area?!!!
                if (interAreaMeas > areaThres) {
                    // Select w1. toggling the selection would work too, but
                    // only because with the current implementation we are
                    // ensuring that at this point w1 will always be
                    // unselected.  It is better to explicitly ask for what we
                    // want: w1 to be selected.
                    if (w1.getRawTimestamp() <= 0) {
                        ds.addSelected(w1);
                    } else if (w2.getRawTimestamp() <= 0) {
                        ds.addSelected(w2);
                    } else if (w1.getRawTimestamp() < w2.getRawTimestamp()) {
                        ds.addSelected(w2);
                    } else {
                        ds.addSelected(w1);
                    }
                    // We want to break out of the inner for loop here to save
                    // processing time, we have already found an intersection
                    // with another building and selected the original
                    // building, no point in examining any other buildings for
                    // intersections.
                    break;
                }
            }
        }
    }

    // The area that is the interesection between the two buildings will not
    // always consist of a single subpath (might be like a multipolygon),
    // however, if both of the buildings are simple polygons, then their
    // intersection will never have any holes (inner members of a multipolygon
    // relation).  Therefore, just make closed ways from all of the subpaths of
    // the area.
    // TODO - have this function return a multipolygon relation, with holes in
    // any, this will be necessary when the tool is enhaced to work with
    // multipolygon buildings.
    private List<Way> waysFromArea(Area a) {
        PathIterator itr = a.getPathIterator(null);
        double[] coords = new double[6];
        List<Way> ways = new ArrayList<>();
        Way w = new Way();
        for (; !itr.isDone(); itr.next()) {
            int segType = itr.currentSegment(coords);
            switch (segType) {
                case PathIterator.SEG_MOVETO:
                case PathIterator.SEG_LINETO:
                    // Coordinates are already in "eastnorth" (vs latlon), but
                    // have to make a new EastNorth object to create the node
                    w.addNode(new Node(new EastNorth(coords[0], coords[1])));
                    break;
                case PathIterator.SEG_CLOSE:
                    // Close the way
                    w.addNode(w.firstNode());
                    ways.add(w);
                    w = new Way();
                    break;
            }
        }
        return ways;
    }
}

---
title: ""
date: 2018-05-06T11:40:10+02:00
---

# Moved to [https://mapathoner.tojemoje.site/][0]

[0]: https://mapathoner.tojemoje.site/

# What if ...
- You create multiple buildings by one way?
- Create residential area by selecting the buildings?

[![Mapathoner demo][1]][6]

See the project on [GitLab][2] or translate on [Transifex][3].

Imagery license [CC-BY 4.0][4] Contributors of [Open Imagery Network][5].

[1]: https://upload.wikimedia.org/wikipedia/commons/9/9f/Mapathoner_demo.gif
[2]: https://gitlab.com/qeef/mapathoner
[3]: https://www.transifex.com/josm/josm/content/
[4]: https://creativecommons.org/licenses/by-sa/4.0/
[5]: https://openimagerynetwork.github.io/
[6]: https://commons.wikimedia.org/wiki/File:Mapathoner_demo.gif

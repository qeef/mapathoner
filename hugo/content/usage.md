---
title: "Usage"
date: 2018-05-06T11:27:29+02:00
---
## How to install
- Launch the ![Preferences][1] **Preferences** dialog.
- Select the tab ![Plugins][2] **Plugins**.
- Click on ![Download list][3] to download the list of available plugins.
- Tick the checkbox for **mapathoner**.
- Click on **OK**.

See the [JOSM wiki][4] for more information.

## How to use
### Batch Circle Building
- Click two nodes on building's diameter (it creates a way).
- Continue clicking another two nodes on the next building's diameter (it
  prolongs the current way).
- **DO NOT FINISH THE WAY!**
- When ready run from menu *Mapathoner -> Batch Circle Building* or associated
  shortcut.

### Batch Orthogonal Building
- Click three nodes on building's corners (it creates a way).
- Continue clicking another three nodes on the next building's corners (it
  prolongs the current way).
- **DO NOT FINISH THE WAY!**
- When ready run from menu *Mapathoner -> Batch Orthogonal Building* or
  associated shortcut.

### Batch L-shaped Building
- Click four nodes on building's "outer" corners (see the picture below).
- The order of the nodes does not matter.
- Continue clicking another four nodes on the next building's "outer" corners.
- **DO NOT FINISH THE WAY!**
- When ready, run from menu *Mapathoner -> Batch L-shaped Building* or
  associated shortcut.

![Mapathoner L-shaped warning][5]

### Pick Residential Area
- Select the buildings the residential area should be around.
- If nothing selected, all the buildings that are currently seen are used.
- Run from menu *Mapathoner -> Pick Residential Area* or associated shortcut.

### Select Duplicate Buildings
- Run *Mapathoner -> Select Duplicate Buildings* or associated shortcut.
- All the buildings that overlap more than 50 % with another building are
  selected.

### Select Non Orthogonal Buildings
- Run *Mapathoner -> Select Non Orthogonal Buildings* or associated shortcut.
- All the buildings that are almost orthogonal are selected.

[1]: https://josm.openstreetmap.de/browser/trunk/images/preference.png?format=raw
[2]: https://josm.openstreetmap.de/browser/trunk/images/preferences/plugin.png?format=raw
[3]: https://josm.openstreetmap.de/browser/trunk/images/download.png?format=raw
[4]: https://josm.openstreetmap.de/wiki/Help/Preferences/Plugins
[5]: https://upload.wikimedia.org/wikipedia/commons/1/17/Mapathoner_L-shaped_warning.png
